VM相比Docker，VM會有浪費資源的情況。
Docker要用多少資源，就用多少資源，不會浪費資源，並且開啟時間短！
Docker可跨平台，用Docker包裝起來後，各種作業系統都能使用！還能減少系統所需環境(re)build時間。

經過本次練習，我能成功使用Docker連接MySQL Workbench，並學習許多不同query的操作！



相關教學連結：
    1.Docker簡介：https://docs.microsoft.com/zh-tw/windows/wsl/tutorials/wsl-containers
    2.Docker連結MySQL：https://www.itread01.com/content/1549114588.html
    3.MySQL基本操作語法：https://tsuozoe.pixnet.net/blog/post/21283890
    4.修改欄位型別或長度：https://www.itread01.com/p/1122614.html
    5.join使用方法：https://www.itread01.com/articles/1478049617.html
    


 其他：解決workbench無法從GUI登入問題(Docker會被阻擋)
    step1.使用cmd進入MySQL後，先create一個user，並授權所有關於原本創的資料庫之權限給這個user！
    step2.修改workbench裡原本創的MySQL connections，將裡面Parameters的username與密碼改成用cmd新創的user！
    
    使用語法：
    CREATE USER 'username'@'%' IDENTIFIED BY 'password';
    grant all on db_name.* to 'username'@'%';

    相關網址
    1.https://github.com/docker-library/mysql/issues/274
    2.https://www.digitalocean.com/community/questions/problem-granting-permission-within-mysql-at-step-6




 這次就先寫到這裡，若所上傳圖片或學習筆記內容有誤，還請多多指教！   
